<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MSValues extends Model
{
    protected $table = 'MSValues';

    public $timestamps = false;
}
