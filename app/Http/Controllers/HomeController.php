<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\MSValues;
use Auth;
use Socialite;
use App\User;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return response()->view('home');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request, [
                'value' => 'required|regex:/^[a-zA-Z0-9]*$/|max:50',
            ]
        );

        try {

            $insertData = new MSValues;

            $insertData->value = $request->input('value');
            
            $insertData->save();

            return redirect('/home')
               ->with('successMessage', 'Value Updated');
        } catch (Exception $e) {
            return redirect('/home')->with(
                'errorMessage', 'Oops Something went wrong, please try again');
        }//end try
    }

    public function fbRedirect(Request $request)
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function fbCallback(Request $request)
    {
        try {
            $user = Socialite::driver('facebook')->user();

            $authUser = User::where('provider_id', $user->id)->first();
            
            if ($authUser) {
                Auth::login($authUser, true);
                return redirect('/home')->with('loginMessage', 'Login succesful');
            }
            
            User::create([
                'name'     => $user->name,
                'email'    => $user->email,
                'provider' => 'facebook',
                'provider_id' => $user->id
            ]);
            Auth::login($user, true);

            return redirect('/home')->with('loginMessage', 'Login succesful');

        } catch (\Exception $e) {
            dd ($e->getMessage());
        }

    }

    public function login(Request $request)
    {
        return view('welcome');
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/')->with('logoutMessage', 'Logout succesful');
    }
    
}
