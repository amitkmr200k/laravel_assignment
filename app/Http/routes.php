<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::get('/fb/redirect', 'HomeController@fbRedirect');
Route::get('/fb/callback', 'HomeController@fbCallback');
Route::get('/logout', 'HomeController@logout');

Route::get('/', 'HomeController@login');

Route::get('/auth/login', 'HomeController@login');


Route::group(['middleware' => ['auth']], function (){
	Route::get('/home', 'HomeController@index');
	Route::post('/home', 'HomeController@store');	
});
