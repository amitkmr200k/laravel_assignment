@extends('master')

@section('body')
    
    <div class="row">
        @if (count($errors) > 0)
                <div class="well">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
        @endif
    </div>
    <div class="row">
            @if(Session::has('loginMessage'))
            <div class="alert alert-success">
                    {{ Session::get('loginMessage') }}
            </div>
            @endif
            @if(Session::has('successMessage'))
            <div class="alert alert-success">
                    {{ Session::get('successMessage') }}
            </div>
            @endif
            @if(Session::has('errorMessage'))
            <div class="alert alert-danger">
                    {{ Session::get('errorMessage') }}
            </div>
            @endif
    </div>
    <div class="row">
        <form action="/home" method="POST">
            <div class="row">
                <input class="form-control" type="text" name="value" placeholder="Enter a value">
                    <input type="hidden" value="{{ csrf_token() }}" name="_token">

            </div>
            <br/>
            <div class="row">
                <div>
                    <input class="btn btn-primary form-control" type="submit" 
                        value="Save" name="submit">
                </div>
                &nbsp;&nbsp;
                <div>
                    <a class="btn btn-success" href="{{ url('/logout') }}">Logout</a>
                </div>
            </div>
        </form>
    </div>
    
@endsection
