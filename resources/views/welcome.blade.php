@extends('master')

@section('body')
    <div class="form-group">
        @if(Session::has('logoutMessage'))
            <div class="alert alert-success">
                    {{ Session::get('logoutMessage') }}
            </div>
            @endif
        <h1> Laravel Assignment</h1>
        <form action="/fb/redirect" method="GET"> 
            <input class="btn btn-primary form-control" type="submit" 
                value="Login with Facbook" name="submit">
        </form>
    </div>
@endsection